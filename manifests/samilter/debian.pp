class sympa::samilter::debian {
  package {
    "spamass-milter":
      ensure => installed;
  }
}
