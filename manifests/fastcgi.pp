class sympa::fastcgi {
  case $operatingsystem {
    debian: { include sympa::fastcgi::debian }
    mageia: { include sympa::fastcgi::mageia }
    default: { include sympa::fastcgi::default }
  }

  file {
    '/var/lib/apache2/fastcgi':
      ensure  => present,
      owner   => root, group => sympa, mode => '0775',
      require => Package['libapache2-mod-fastcgi'];

    '/var/lib/apache2/fastcgi/dynamic':
      ensure  => present,
      owner   => root, group => sympa, mode => '0770',
      require => File['/var/lib/apache2/fastcgi'];

    # FIXME: is this still needed?
    # I dont know, but I changed it from apache2::envvars to a file resource
    # in order to switch from the old apache module to the new one (which
    # doesn't support envvars)
    '/etc/apache2/envvars'
     content => "export APACHE_PID_FILE=/var/run/apache2.pid\nexport APACHE_RUN_USER=sympa\nexport APACHE_RUN_GROUP=sympa\nexport APACHE_RUN_DIR=/var/run/apache2$SUFFIX\nexport APACHE_LOCK_DIR=/var/lock/apache2$SUFFIX\nexport APACHE_LOG_DIR=/var/log/apache2$SUFFIX\n\nexport LANG=C\nexport LANG\n"
  }

  apache::module {
    'fastcgi': ensure => present, package_name => 'libapache2-mod-fastcgi';
    'fcgid': ensure => absent;
  }

}

