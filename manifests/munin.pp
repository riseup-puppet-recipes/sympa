# sympa munin plugins
# some of these plugins need database access, ensure that the munin user
# has access by giving that user a .my.cnf file that gives them read-only
# access to whereever the database is
# FIXME right now this class has some hard coded paths to the sympa spool,
#  logs, etc.

class sympa::munin {

  file {
    '/usr/local/share/munin-plugins/sympa_queue':
      source => 'puppet:///modules/sympa/munin/sympa_queue',
      mode   => '0755',
      owner  => root,
      group  => root;
    
    '/usr/local/share/munin-plugins/sympa_stats':
      source => 'puppet:///modules/sympa/munin/sympa_stats',
      mode   => '0755',
      owner  => root,
      group  => root;
    
    '/usr/local/share/munin-plugins/sympa_subscribers':
      source => 'puppet:///modules/sympa/munin/sympa_subscribers',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/share/munin-plugins/sympa_lists':
      source => 'puppet:///modules/sympa/munin/sympa_lists',
      mode   => '0755',
      owner  => root,
      group  => root;

    '/usr/local/share/munin-plugins/sympa_users':
      source => 'puppet:///modules/sympa/munin/sympa_users',
      mode   => '0755',
      owner  => root,
      group  => root;
  }
  
  munin::plugin {
    # only needs to run ps, so can run as munin
    'sympaps':
      ensure => 'multips',
      config => "user munin\nenv.names wwsympa sympa archived task_manager bounced bulk\nenv.regex_sympa ^[0-9]* sympa.pl\nenv.regex_archived ^[0-9]* archived.pl\nenv.regex_task_manager ^[0-9]* task_manager.pl\nenv.regex_bounced ^[0-9]* bounced.pl\nenv.regex_bulk ^[0-9]* bulk.pl\n";
  }

  munin::plugin {
    # only needs to run find in spool/ dirs, so can run as munin
    'sympa_queue':
      ensure         => 'sympa_queue',
      config         => "user munin\nenv.spooldir /home/sympa/spool\n",
      script_path_in => '/usr/local/share/munin-plugins';
  }
  
  munin::plugin {
    # needs to be able to read logs in /var/log/sympa
    'sympa_stats':
      ensure         => 'sympa_stats',
      config         => "user root\nenv.logfile /var/log/sympa/sympa.log\n",
      script_path_in => '/usr/local/share/munin-plugins';
  }
  
  munin::plugin {
    # needs db access, can run as munin
    'sympa_subscribers':
      ensure         => 'sympa_subscribers',
      config         => "user munin\n",
      script_path_in => '/usr/local/share/munin-plugins';
  }
  
  munin::plugin {
    # needs db access, can run as munin
    'sympa_lists':
      ensure         => 'sympa_lists',
      config         => "user munin\n",
      script_path_in => '/usr/local/share/munin-plugins';
  }

  munin::plugin {
    # needs db access, can run as munin
    'sympa_users':
      ensure         => 'sympa_users',
      config         => "user munin\n",
      script_path_in => '/usr/local/share/munin-plugins';
  }
}
