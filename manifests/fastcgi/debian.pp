class sympa::fastcgi::debian {
  package {
    [ libapache2-mod-fastcgi, libcgi-fast-perl ]:
      ensure => installed;
  }
}
