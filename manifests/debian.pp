class sympa::debian (
  $upstream = false
) {

  if $upstream {
    # if the above is set, we depend on all the packages ourselves, otherwise
    # we just depend on the sympa package which pulls everything in

    # these come from the debian package dependencies (apt-cache show sympa)
    # some things in here we don't need, but are there for completeness

    # other dependencies we don't specify: mail-transport-agent, apache,
    #  rsyslog, lsb-base, adduser, ca-certificates

    # perl related dependencies, in order specified by debian package (to make
    #   comparing in the future easier
    # things that are commented out we don't yet have includes for
    # also, consult upstream src/lib/Sympa/ModDef.pm for more info on why things are included
    include perl::extensions::archive_zip
    include perl::extensions::mailtools
    #libmsgcat-perl - unsure, something about locale files, only needed at build time?
    include perl::extensions::mime_tools
    include perl::extensions::net_dns
    #libclass-singleton-perl - not currently on whimbrel
    #libdatetime-format-mail-perl - not currently on whimbrel
    #libfile-nfslock-perl - not currently on whimbrel
    include perl::extensions::mail_dkim
    #libnet-cidr-perl
    #libcrypt-openssl-x509-perl - not currently on whimbrel, needed for S/MIME and SSL
    #libcrypt-smime-perl - needed for S/MIME
    include perl::extensions::io_stringy
    #libnet-ldap-perl - not currently on whimbrel, needed for LDAP
    #libcgi-fast-perl - needed for FCGI?
    include perl::extensions::intl
    include perl::extensions::mime_charset
    include perl::extensions::mime_encwords
    include perl::extensions::template
    include perl::extensions::libxml_libxml
    include perl::extensions::cgi_pm
    include perl::extensions::html_stripscripts_parser
    include perl::extensions::html_tree
    include perl::extensions::html_format
    include perl::extensions::regexp_common
    include perl::extensions::fcgi
    include perl::extensions::file_copy_recursive
    include perl::extensions::net_netmask
    include perl::extensions::term_progressbar
    include perl::extensions::mime_lite_html
    include perl::extensions::unicode_linebreak
    include perl::extensions::soap_lite
    include perl::extensions::crypt_ciphersaber
    include perl::extensions::io_socket_ssl

    # starting in 6.2 we need a bunch of jquery stuff
    #fonts-font-awesome
    #libjs-jquery (>= 1.11)
    #libjs-jquery-migrate-1
    #libjs-jquery-ui
    #libjs-jquery-placeholder
    #libjs-modernizr
    #libjs-twitter-bootstrap

    # right now we assume mysql, if anyone ever needs a different one we can add support
    # also we count on libdbi-perl getting pulled in as a dependency
    include perl::extensions::dbd_mysql
    
    package {
      'mhonarc':
        ensure => installed;

      # since we're using upstream, ensure that the sympa package is purged
      "sympa":
        ensure => purged;
    }
  } else {
    package { "sympa": ensure => installed; }
  }
}
