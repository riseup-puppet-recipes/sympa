class sympa (
  $cgitype  = 'fcgid',
  $munin    = true,
  $samilter = true,
  $upstream = false
) {

  case $operatingsystem {
    debian: { class { 'sympa::debian': upstream => $upstream } }
    mageia: { include sympa::mageia }
    default: { err('This operating system not supported') }
  }

  if $munin { include sympa::munin }
  if $samilter { include sympa::samilter }

  case $cgitype {
    fcgid: { include sympa::fcgid }
    fastcgi: { include sympa::fastcgi }
    default: { err('This CGI type not supported') }
  }

  file { '/etc/sympa/sympa.conf':
    ensure  => present,
    # should be cleaner to have it root owned, but puppet do not support acl
    # and in any case, config will be reset if it change
    owner   => sympa,
    group   => sympa,
    mode    => '0640',
    content => template('site_sympa/sympa.conf.erb'),
    require => Package[sympa],
  }

  augeas {
    "logrotate_sympa":
      context => "/files/etc/logrotate.d/sympa/rule",
      changes => [ "set file /var/log/sympa/*.log", "set rotate 3",
        "set schedule weekly", "set compress compress",
        "set missingok missingok", "set ifempty notifempty",
        "set copytruncate copytruncate" ]
  }
}
