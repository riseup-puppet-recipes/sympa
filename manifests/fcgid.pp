class sympa::fcgid {
  case $operatingsystem {
    debian: { include sympa::fcgid::debian }
    mageia: { include sympa::fcgid::mageia }
    default: { include sympa::fcgid::default }
  }


  # if you need to work around the bird timeout, use this. Default is 40.
  # FcgidIOTimeout 300'
  class { 'apache::mod::fcgid':
    options => {
      'AddHandler'                 => 'fcgid-script .fcgi',
      'FcgidMaxRequestLen'         => '33554432',
      'FcgidMaxRequestsPerProcess' => '1500',
      'FcgidMaxProcesses'          => '400',
      'FcgidMaxProcessesPerClass'  => '400',
      'FcgidIOTimeout'             => '240',
      'FcgidIdleTimeout'           => '60',
      'FcgidIdleScanInterval'      => '30',
      'FcgidBusyTimeout'           => '120',
      'FcgidBusyScanInterval'      => '90',
      'FcgidErrorScanInterval'     => '3',
      'FcgidZombieScanInterval'    => '1',
      'FcgidProcessLifeTime'       => '120'
    },
  }
}

