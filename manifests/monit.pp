class sympa::monit {

# FIXME: we don't have as much memory pressure as we used to, these
# don't fire anymore, but maybe still useful? if so they will need to be
# fastcgi/fcgid aware.

  #if defined(Class['monit']) {
  #  monit::check::process { "wwsympa.fcgi":
  #    pidfile => "/var/run/apache2.pid",
  #    start => "/etc/init.d/apache2 start",
  #    stop => "/etc/init.d/apache2 stop",
  #    customlines => ["if totalmem > 5000 MB for 3 cycles then restart"];
  #  }
  #
  #  monit::check::process {"task_manager.pl":
  #    pidfile => "/home/sympa/task_manager.pid",
  #    start => "/etc/init.d/sympa start",
  #    stop => "/etc/init.d/sympa stop",
  #    customlines => ["if totalmem > 5000 MB for 3 cycles then restart"],
  #    ensure => absent;
  #  }
  #}

}
