class sympa::samilter {
  case $::operatingsystem {
    debian: { include sympa::samilter::debian }
    default: { include sympa::samilter::default }
  }

  file {
    '/var/run/sendmail':
      ensure => directory,
      mode   => '0777',
      owner  => postfix,
      group  => postfix;

    '/var/run/spamass/spamass.sock':
      ensure => present,
      mode   => '0755',
      owner  => postfix,
      group  => postfix;

    # get config file from site_sympa since there is no generic default
    '/etc/default/spamass-milter':
      source  => 'puppet:///modules/site_sympa/spamassassin/spamass-milter_default',
      require => Package['spamass-milter'],
      mode    => '0644',
      owner   => root,
      group   => root;
  }

  service {
    'spamass-milter':
      ensure     => running,
      name       => spamass-milter,
      pattern    => '/usr/sbin/spamass-milter',
      enable     => true,
      hasrestart => true,
      subscribe  => File['/etc/default/spamass-milter'],
      require    => File['/etc/default/spamass-milter'];
  }
}
